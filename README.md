# Sasch Alarm - Door Guard

Kleine Alarmanlage, die mittels eines Reed Kontakt an der Tür beim
Öffnen mit dem Piezo Alarm macht.
Der Reed Kontakt wird durch einen Raspberry Pi Pico
Microcontroller überwacht und schaltet den Piezo Summer an, wenn
die Tür geöffnet wird.

## Bedienungsanleitung:
Das Gerät wird mit 3 Batterien betrieben. Am Batteriefach ist ein
Schalter, mit dem man das Gerät ein- und ausschalten kann.
Es gibt eine rote Leuchtdiode und eine grüne Leuchtdiode:
- Rote LED:
zeigt an ob der Alarm an ist oder nicht. (Zum testen)
- grüne LED:
zeigt an, ob der Piezo Summer aktiviert ist oder nicht. Mit
dem Taster kann man den Piezo Summer aktivieren oder
deaktivieren.  
  
Zum testen des Gerätes kann man den Piezo Summer deaktivieren
und beim Öffnen und Schließen der Tür anhand der roten LED
sehen, ob der Alarm funktioniert.
  
